# Patches for Thunderbird

We apply some patches on top of Thunderbird for various reasons.  This
documents the patches.

## comm-fix-mpis-of-encrypted-keys.patch

Fixes test vectors that were created with a version of GnuPG that
emits malformed MPIs.

https://bugzilla.mozilla.org/show_bug.cgi?id=1953189

Applied to:
  - esr115
  - esr128
  - beta

## comm-fix-v6-expectation.patch

The test suite asserts that v6 certs are rejected.  Adjust that
expectation.

Applied to:
  - esr128
  - beta

## ignore_test.patch

Ignore a test (browser_keyWizard.js) that is failing in our CI with
RNP.

Applied to:
  - esr128
  - beta

## increase_mochitest_timeout.patch

Increase mochitest timeouts.

This makes it less likely for the tests to fail in our CI.

Applied to:
  - esr115
  - esr128
  - beta

## normalize-armor-line-endings.patch

Don't overfit on RNP's Windows line endings.

Applied to:
  - esr115
  - esr128
  - beta

## artifact-comm-esr115.patch

Fixes the artifact build.  Applied to esr115.

## artifact-comm-esr128.patch

Fixes the artifact build.  Applied to esr128.
